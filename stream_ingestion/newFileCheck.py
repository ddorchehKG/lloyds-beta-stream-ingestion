
import os, time
from google.cloud import storage
from google.cloud import pubsub_v1
import argparse
import sys
import send_bytes as sb
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./de22-329512-f06e2cd71add.json"


def parse_command_line_args():
    """Process commandline arguments

    Returns:
        Namespace: Arguments and their set values.
    """
    parser = argparse.ArgumentParser(description=(
            'Example Google Cloud IoT Core MQTT device connection code.'))
    parser.add_argument(
            '--project_id',
            default="de22-329512",
            help='GCP cloud project name')
    parser.add_argument(
            '--sender_info',
            default='simulated ingestion server',
            help='Name of publisher')
    parser.add_argument(
            '--topic_id',
            default='audio_stream', 
            help='PubSub topic')
    parser.add_argument(
            '--cloud_region', default='europe-west2', help='GCP cloud region')
    return parser.parse_args()

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to a specified bucket on GCP

    Args:
        bucket_name (str): 
            Existing bucket name on google cloud storage.
            
        source_file_name (str): 
            Name of source file.
            
        destination_blob_name (str):
            Name of destination file.
    """
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    return(
        "File {} uploaded to {}.".format(
            source_file_name, destination_blob_name
        )
    )


class Watcher:
    """Monitors target folder for new files being created.
    """

    def __init__(self, directory="beta_target", handler=FileSystemEventHandler()):
        self.observer = Observer()
        self.handler = handler
        self.directory = directory

    def run(self):
        self.observer.schedule(
            self.handler, self.directory, recursive=True)
        self.observer.start()
        print("\nWatcher Running in {}/\n".format(self.directory))
        try:
            while True:
                time.sleep(1)
        except:
            self.observer.stop()
        self.observer.join()
        print("\nWatcher Terminated\n")


class MyHandler(FileSystemEventHandler):
    def on_any_event(self, event):
        """On new file creation, upload to buckt storage if file is bigger than 10Mb (PubSub max message size)
            or if the file needs converting to different format. If the file is valid and the paylaod is less than 10Mb
            a oubsub message will be sent.       


        Args:
            event (str): 
                Type of event to tak action on (e.g. creation, modification, etc)
        """
        if event.event_type == 'created':
            filetype = os.path.basename(event.src_path).split('.')[1]
            if filetype in allowed_audio_format:
                payload = sb.make_payload(event.src_path)
            else:
                payload = False
            if sys.getsizeof(payload) > 9e6 or payload == False:
                blob_name = os.path.basename(event.src_path) #remove sub directories
                if payload:
                    print("{} too big to PubSub, uploading to {}".format(os.path.basename(event.src_path), blob_name))
                elif filetype in allowed_video_format:
                    print("{} detected video format, uploading to {}".format(os.path.basename(event.src_path),blob_name))
                else:
                    print("{} requires conversion, uploading to {}".format(os.path.basename(event.src_path), blob_name))
                upload_blob(bucket_name, event.src_path, blob_name)
                print("Done")
            else:
                print('Publishing {} in bytes...'.format(os.path.basename(event.src_path)))
                # # # When you publish a message, the client returns a future.
                future = publisher.publish(topic_path, payload)
                if future.result():
                    print(future.result())
                    print(f"Published messages to {topic_path}.")
                else:
                    print('Message did not send')

if __name__=="__main__":
    allowed_video_format = ['avi', 'm4v', 'mp4']
    allowed_audio_format = ['mp3', 'm4a']
    args = parse_command_line_args()
    bucket_name = 'media_landing'
    project_id = args.project_id
    topic_id = args.topic_id
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_id)

    w = Watcher(".", MyHandler())
    w.run()
