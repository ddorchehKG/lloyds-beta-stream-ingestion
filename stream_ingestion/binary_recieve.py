from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
import io
from pydub import AudioSegment
import json
import base64
import os
import gzip

project_id = "de22-329512"
topic_id = "betastream"
subscription_id = "events"

subscriber = pubsub_v1.SubscriberClient()
# The `subscription_path` method creates a fully qualified identifier
# in the form `projects/{project_id}/subscriptions/{subscription_id}`
subscription_path = subscriber.subscription_path(project_id, subscription_id)

def callback(message: pubsub_v1.subscriber.message.Message) -> None:
    print("Received.")
    recieved = base64.b64decode(message.data)
    decompressed_payload = gzip.decompress(recieved)
    payload_json = json.loads(decompressed_payload)
    filename = payload_json['filename']
    filename_new = filename.split('.')[0] + '.mp3'
    encoded2 = base64.b64decode(payload_json['byte_string'])

    recording_new = AudioSegment.from_file(io.BytesIO(encoded2), format = 'raw',
                                frame_rate=44100, channels=2, 
                                sample_width=2)
    #recording_new.export(filename_new, 'mp3')
    print('saved new file in {} as {}'.format(os.getcwd(), filename_new))
    print(filename_new)
    message.ack()

streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
print(f"Listening for messages on {subscription_path}..\n")

# Wrap subscriber in a 'with' block to automatically call close() when done.
with subscriber:
    try:
        # When `timeout` is not set, result() will block indefinitely,
        # unless an exception is encountered first.
        streaming_pull_future.result(timeout=1000)
    except TimeoutError:
        streaming_pull_future.cancel()  # Trigger the shutdown.
        streaming_pull_future.result()  # Block until the shutdown is complete.

