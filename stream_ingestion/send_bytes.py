import io
from pydub import AudioSegment
import json
import base64
import gzip
import os


def make_payload(filepath):
    """Opens an audio file, writes contents as a string of bytes in a json with the 
    source filename. The json encoded in base64 and compressed with gzip to allow for 
    more efficient publishing of pubsub messages.

    Args:
        filepath (str): 
            Source file to be opened.
    Returns:
        bytes: 
            Compressed bytes to be sent in a PubSub.
    """
    filename = os.path.basename(filepath) 
    file_type = os.path.splitext(filename)[1][1:]
    data=open("{}".format(filepath),"rb").read()
    recording = AudioSegment.from_file(io.BytesIO(data), format=file_type, 
                                    frame_rate=44100, channels=2, 
                                    sample_width=2)#

    bytes_string = recording.raw_data
    encoded = base64.b64encode(bytes_string)
    record = {
        'filename': filename,
        'byte_string': encoded.decode('utf-8')
    }
    payload = json.dumps(record).encode("utf-8")
    compressed_payload = gzip.compress(payload)
    to_send = base64.b64encode(compressed_payload)
    return to_send


