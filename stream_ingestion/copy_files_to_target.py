import os 
from shutil import copy2
import time
import random

accepted_file_types = ['mp3', 'm4a', 'wav', 'avi, m4v', 'mp4', 'flac', 'ogg']

def sim_new_files(max_sleep_time = 10):
    """Randomly moves a file from one file store to a target folder, to similiate new files
    that need to be processed.

    Args:
        max_sleep_time (int, optional): Defaults to 2.
            The max time between file transfers.
    """
    if len(os.listdir('beta_samples')) == 0:
        return("No valid files in sample folder.")
    else:
        for file in os.listdir('beta_samples'):
            file_type = os.path.splitext(file)[1][1:]
            if file_type in accepted_file_types:
                copy2(f'beta_samples/{file}', 'beta_target')
                print(f'{file} has been copied to beta_target')
                random_sleep_time = random.randint(1,max_sleep_time)
                time.sleep(random_sleep_time)
                
if __name__ == '__main__':
    sim_new_files()
                

        