import io
from pydub import AudioSegment
from google.cloud import pubsub_v1
import json
import base64
import gzip

filename = "Welcome.mp3"
data=open("beta_samples/{}".format(filename),"rb").read()
recording = AudioSegment.from_file(io.BytesIO(data), format="mp3", 
                                   frame_rate=44100, channels=2, 
                                   sample_width=2).remove_dc_offset()

bytes_string = recording.raw_data
encoded = base64.b64encode(bytes_string)
record = {
    'filename': filename,
    'byte_string': encoded.decode('utf-8')
}
payload = json.dumps(record).encode("utf-8")
compressed_payload = gzip.compress(payload)
#to_send = base64.b64encode(compressed_payload)


project_id = "de22-329512"
topic_id = "betastream"

publisher = pubsub_v1.PublisherClient()
# # The `topic_path` method creates a fully qualified identifier
# # in the form `projects/{project_id}/topics/{topic_id}`
topic_path = publisher.topic_path(project_id, topic_id)

# # # When you publish a message, the client returns a future.
future = publisher.publish(topic_path, b'test')
print(future.result())

# print(f"Published messages to {topic_path}.")

# decompressed_payload = gzip.decompress(compressed_payload)
# recieved = decompressed_payload.decode("utf-8")
# payload_json = json.loads(payload)
# filename = payload_json['filename']
# filename_new = filename.split('.')[0] + '.mp3'
# decoded = base64.b64decode(payload_json['byte_string'])

# recording_new = AudioSegment.from_file(io.BytesIO(decoded), format = 'raw',
#                                 frame_rate=44100, channels=2, 
#                                 sample_width=2)
# recording_new.export(filename_new, 'mp3')
